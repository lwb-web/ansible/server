# Provision: Linux-Server

Folgende Server werden mit dem Playbooks installiert:

* wolf.lwb.local

Das Ausrollen erfolgt aus auf Basis von [Ansible](https://www.ansible.com/). Mit der Installation wird einen Cronjob erstellt, der per [ansible-pull](https://docs.ansible.com/ansible/latest/cli/ansible-pull.html) dieses Repository regelmäßig auf den Server anwendet. Damit ist der direkte Zugriff auf den Server im Betrieb nicht notwendig.

## Voraussetzungen

* Die Playbooks wurden für [Debian](https://www.debian.org/index.de.html) und [Ubuntu](https://ubuntu.com/) geschrieben und getestet.
* Eine aktuelle Installation eines der genannten Betriebssysteme ist notwendig.
* Ein Eintrag im DNS muss für den Server existieren.
	* Debian: `/etc/network/interfaces`
	* Ubuntu: `/etc/netplan/00-installer-config.yaml`
* Folgende Software muss zwingend installiert sein:
 	* git
	* ansible
* Folgende Datei muss angelgt werden, die das Passwort zum Entschlüsseln der [VAULT-Dateien](https://docs.ansible.com/ansible/latest/user_guide/vault.html) enthält. Das Passwort ist im Passwort-Save hinterlegt.
	* /root/ansible.pwd

## Installation

* Debian: `apt update && apt install git ansible -y && ansible-pull -U https://gitlab.com/lwb-web/ansible/server.git`
* Ubuntu: `sudo apt update && sudo apt install git ansible -y && sudo ansible-pull -U https://gitlab.com/lwb-web/ansible/server.git`

## Rollen

Folgende Rollen wurde vorbereitet. Die entsprechende Rolle muss in der Datei für den Server hinterlegt werden.

* [common](roles/common)
* [applications](roles/applications)
* [docker](roles/docker)
* [docker-compose](roles/docker-compose)
* [gitlab](roles/gitlab)
* [gitlab-runner](roles/gitlab-runner)
* [kubernetes](roles/kubernetes)

## Zuätzliche Software für wolf

* [Log-Service](https://gitlab.com/lwb-web/docker/log)
* [Synchronisation WBS mit Lecos](http://version.lwb.local/sap/schnittstellen/sync_wbs)
* [Synchronisation mit Sozialamt](http://version.lwb.local/schnittstelle/sync_sozialamt)
* [Synchronisation HR](http://version.lwb.local/schnittstelle/mitarbeiterabgleich/sync_rexx)
