# Docker-Compose

Alle auf dem Server installierten Applikationen basieren auf einem [Template](https://gitlab.com/lwb-web/templates/docker-compose), das [Docker-Compose](https://docs.docker.com/compose/) als zwingende Voraussetzung hat. Folgende Cronjobs werden installiert, in die sich die Applikationen "einklinken" können. Die Beschreibung dafür befindet sich in der Dokumentation zum [Template](https://gitlab.com/lwb-web/templates/docker-compose).

## Features

* cron: Start alle 10 Minuten
* cron-daily-morning: Start täglich 06:00 Uhr
* cron-daily-evening: Start täglich 20:20 Uhr
* backup: Start täglich 21:00 Uhr
* update: Start Sonnabends 00:00 Uhr
