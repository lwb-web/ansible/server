# Gitlab-Runner

Installation einer [Gitlab-Runner](https://docs.gitlab.com/runner/install/linux-manually.html) Instanz auf Betriebssystem-Ebene.

Dieser Runner dient zum Start von Befehlen auf Shell-Ebene z.B. für das Ausrollen von Anpassungen oder zur Installation von Anwendungen auf Basis vom [Template](https://gitlab.com/lwb-web/templates/docker-compose).

Der Gitlab-Runner dient als Bindeglied zwischen Versionsverwaltung ([intern](http://verion.lwb.local) wie [extern](https://gitlab.com)) und dem jeweiligen Server.

## Features

* führt lokal Skripte aus
* hat Zugriff auf Docker bzw. Docker-Compose
	* Docker-Container aus Docker-Images bauen
	* Ausführen von Tests
