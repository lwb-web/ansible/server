# Common

Allgemeine Rolle, die in allen Servern installiert werden solle.

## Features
### Ansible

* Anlage eines System-Users `ansible`
* Installation eines Cron-Jobs, der dieses Repository alle 10 Minuten ausführt

### Basics

Installation von:
* git
* open-vm-tools
* cifs-utils
* unzip

### Autoupdate

* Einspielen aller Updates