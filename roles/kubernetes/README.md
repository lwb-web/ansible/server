# Kubernetes

Auf dem Server wird die Kubernetes-Distribution [K3S](https://k3s.io/). Es ensteht lediglich ein Single-Node-Cluster, der aber für die Belange der LWB ausreichend ist.

## Grundinstallation

* Installation K3S (Kubernetes): `curl -sfL https://get.k3s.io | sudo K3S_KUBECONFIG_MODE="644" INSTALL_K3S_VERSION=v1.25.2+k3s1 sh -s - --disable servicelb`

Die Modifikation des Clusters erfolgt [automatisch](https://rancher.com/docs/k3s/latest/en/helm/#:~:text=and%20Helm%20Charts-,link,-Any%20Kubernetes%20manifests):

* [cert-manager](https://cert-manager.io/docs/configuration/ca/): Er übernimmt die Verwaltung der Zertifikate und stellt sie automatisch für die installierten Applikationen zur Verfügung. Die Zertifizierungstelle basiert auf einem LWB-eigenen Zertifikat. Weiterleitung zu https-Protokoll über Traefik-Middleware
* [metallb](https://metallb.universe.tf/) übernimmt die AUfgabe des internen LoadBalancers und wurde auf die IP 10.100.133.144 konfiguriert. Der in K3S enthaltene LoadBalancer wurde daher deaktiviert.
* Windows-Freigabe `backup1`: Ziel für Backups und als Quelle für Importe, [Treiber](https://github.com/kubernetes-csi/csi-driver-smb) wird installiert
* Windows-Freigaben `ara2`:  Veröffentlichung über einen Webserver, [Treiber](https://github.com/kubernetes-csi/csi-driver-smb) wird installiert
* [ArgoCD](https://argo-cd.readthedocs.io/): Dient zum Ausrollen der Kubernetes Deployments

## Features
### Deployment über ArgoCD

Sofern es die Anwendungen ermöglichen, werden sie [ArgoCD](https://argocd.web1.lwb.local) ausgerollt. Dazu wurde ein [Repository](https://gitlab.com/lwb-web/docker/cluster) eingericht, was auf die zu installierenden Applikationen verweist und diese über ArgoCD installiert. Grundsätzliches und nicht öffentliches dazu, wie Namespaces oder Secrets, werden im [Ordner](roles/kubernetes/files/SERVER-NAME/) hinterlegt und dadurch automatisch installiert.

### Gitlab-Runner

Eine Instanz wird im Kubernetes-Cluster installiert und ist das Bindeglied zwischen Versionsverwaltung [extern](https://gitlab.com) und dem Cluster.